package com.rrobbott.android.example.preferencefragmentandbackupapi;

import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;

public class BackupAgent extends BackupAgentHelper {
	
	private static final String KEY_PREFIX_SHARED_PREFERENCES_BACKUP_HELPER = "sharedPreferencesBackupHelper"; 
	
	@Override
	public void onCreate() {
				
		// Create backup helper for shared preferences and install it. Since PreferenceFragment uses the default
		// shared preferences we somehow need to get its name; constructing it (based on how Android stores the
		// default preferences on file.
		SharedPreferencesBackupHelper sharedPreferencesBackupHelper =
			new SharedPreferencesBackupHelper(this, getPackageName() + "_preferences");
		
		addHelper(KEY_PREFIX_SHARED_PREFERENCES_BACKUP_HELPER, sharedPreferencesBackupHelper);
	}
}

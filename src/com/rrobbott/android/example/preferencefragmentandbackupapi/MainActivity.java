package com.rrobbott.android.example.preferencefragmentandbackupapi;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Initialize preferences with default values, if any.
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        
        //
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		// Handle item selection.
		switch (item.getItemId()) {
			case (R.id.menu_settings):
				
				// Display settings.
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
				
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}

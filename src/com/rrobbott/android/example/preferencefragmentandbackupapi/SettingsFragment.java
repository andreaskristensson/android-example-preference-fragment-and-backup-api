package com.rrobbott.android.example.preferencefragmentandbackupapi;

import android.app.backup.BackupManager;
import android.os.Bundle;
import android.preference.PreferenceFragment;

public class SettingsFragment extends PreferenceFragment {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Load preferences from XML resource.
		addPreferencesFromResource(R.xml.preferences);
	}

	@Override
	public void onStop() {
		// 
		super.onStop();
		
		// Request backup, data might have changed.
		BackupManager.dataChanged("com.rrobbott.android.example.preferencefragmentandbackupapi");
	}
}

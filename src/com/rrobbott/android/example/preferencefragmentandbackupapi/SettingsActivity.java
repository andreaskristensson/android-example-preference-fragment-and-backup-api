package com.rrobbott.android.example.preferencefragmentandbackupapi;

import android.app.Activity;
import android.os.Bundle;

public class SettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Display settings fragment as main content.
		getFragmentManager().
			beginTransaction().
			replace(android.R.id.content, new SettingsFragment()).
			commit();
	}
}
